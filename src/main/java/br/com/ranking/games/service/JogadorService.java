package br.com.ranking.games.service;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import br.com.ranking.games.model.Jogador;
import br.com.ranking.games.repository.JogadorRepository;

@Service
public class JogadorService {
    
    @Autowired
    JogadorRepository repository;

    public ResponseEntity<List<Jogador>> getAll(){
        return ResponseEntity.ok(repository.findAll(Sort.by(Sort.Order.desc("vitorias"), Sort.Order.asc("nome"))));
    }

    public ResponseEntity<?> create(@Valid @RequestBody Jogador newJogador){

        newJogador.setId(null);
        if(newJogador.getVitorias() == null)
            newJogador.setVitorias(0);
        if(newJogador.getPartidas() == null)
            newJogador.setPartidas(0);
        if(newJogador.getVitorias() > newJogador.getPartidas()){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Número de vitórias ["+
            newJogador.getVitorias().toString()+"] não pode ser maior que número de partidas ["+
            newJogador.getPartidas().toString()+"]");
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(repository.save(newJogador));
    }

    public ResponseEntity<?> update(@Valid @RequestBody Jogador newJogador){

        Optional<Jogador> oldJogador = repository.findById(newJogador.getId());
        if(oldJogador.isPresent()){
            if(newJogador.getVitorias() == null)
                newJogador.setVitorias(0);
            if(newJogador.getPartidas() == null)
                newJogador.setPartidas(0);
            if(newJogador.getVitorias() > newJogador.getPartidas()){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Número de vitórias ["+
                newJogador.getVitorias().toString()+"] não pode ser maior que número de partidas ["+
                newJogador.getPartidas().toString()+"]");
            }
            return ResponseEntity.status(HttpStatus.CREATED).body(repository.save(newJogador));
        }else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Não foi encontrado jogador com ID: "+newJogador.getId().toString());
        }
        
    }

    public ResponseEntity<?> delete(Integer id){

        Optional<Jogador> oldJogador = repository.findById(id);
        if(oldJogador.isPresent()){
            repository.deleteById(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("");
        }else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Não foi encontrado jogador com ID: "+id.toString());
        }
        
    }

    public ResponseEntity<?> addVitoriasAndPartidas(Integer id, Integer qtdeVitorias, Integer qtdePartidas){

        if(qtdeVitorias == null)
            qtdeVitorias = 0;
        if(qtdePartidas == null)
            qtdePartidas = 0;
        if(id == null)
            id = 0;

        Optional<Jogador> find = repository.findById(id);

        if(find.isPresent()){
            Jogador jogador = find.get();
            jogador.setVitorias(jogador.getVitorias()+qtdeVitorias);
            jogador.setPartidas(jogador.getPartidas()+qtdePartidas);
            if(jogador.getVitorias() > jogador.getPartidas()){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Número de vitórias ["+
                jogador.getVitorias().toString()+"] não pode ser maior que número de partidas ["+
                jogador.getPartidas().toString()+"]");
            }
            return ResponseEntity.status(HttpStatus.CREATED).body(repository.save(jogador));
        }else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Não foi encontrado jogador com ID: "+id.toString());
        }
        
    }
}
