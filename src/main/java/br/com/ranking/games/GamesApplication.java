package br.com.ranking.games;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import br.com.ranking.games.model.Jogador;
import br.com.ranking.games.repository.JogadorRepository;

@SpringBootApplication
public class GamesApplication {

	public static void main(String[] args) {
		SpringApplication.run(GamesApplication.class, args);
	}

	@Bean
	public CommandLineRunner setup(JogadorRepository repository){
		return (args) -> {
			repository.save(new Jogador(1, "Jogador 1", 10, 5));
		};
	}

}
