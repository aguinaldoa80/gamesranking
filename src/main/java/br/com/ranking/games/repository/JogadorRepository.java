package br.com.ranking.games.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.ranking.games.model.Jogador;

public interface JogadorRepository extends JpaRepository<Jogador, Integer> {

}
