package br.com.ranking.games.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.ranking.games.model.Jogador;
import br.com.ranking.games.service.JogadorService;

@CrossOrigin("*")
@RestController
@RequestMapping(value = "/api/v1/jogadores")
public class JogadorController {

    @Autowired
    JogadorService service;

    @GetMapping
    public ResponseEntity<List<Jogador>> getAll(){

        return service.getAll();
    } 

    @PostMapping
    public ResponseEntity<?> create(@RequestBody Jogador newJogador){

        return service.create(newJogador);
    } 

    @PutMapping
    public ResponseEntity<?> update(@RequestBody Jogador newJogador){

        return service.update(newJogador);
    } 

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id){

        return service.delete(id);
    } 

    @PutMapping("/{id}/{qdteVitorias}/{qdtePartidas}")
    public ResponseEntity<?> addVitoriasAndPartidas(@PathVariable Integer id, Integer qtdeVitorias, Integer qtdePartidas){

        return service.addVitoriasAndPartidas(id, qtdeVitorias, qtdePartidas);
    }
    
}
