package br.com.ranking.games.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import br.com.ranking.games.model.Jogador;

@SpringBootTest
@AutoConfigureMockMvc
public class JogadorControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    private String urlBase = "/api/v1/jogadores";

    @Test
    void getAll() throws Exception {
    
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(this.urlBase);
        mockMvc.perform(requestBuilder).andExpect(status().isOk());
    }

    @Test
    void create() throws Exception {
    
        Jogador jogador = new Jogador(null, "Teste Mock", 1, 1);
        mockMvc.perform(post(this.urlBase)
        .contentType("application/json")
        .content(objectMapper.writeValueAsString(jogador)))
        .andExpect(status().isCreated());
    }

    @Test
    void update() throws Exception {
    
        Jogador jogador = new Jogador(26, "Teste Mock II", 5, 5);
        mockMvc.perform(put(this.urlBase)
        .contentType("application/json")
        .content(objectMapper.writeValueAsString(jogador)))
        .andExpect(status().isCreated());
    }

    @Test
    void remover() throws Exception {
    
        Integer id = 19;
        mockMvc.perform(delete(this.urlBase+"/"+id.toString()))
        //.andExpect(status().isNoContent());
        .andExpect(status().isNotFound());
    }
    
    @Test
    void addVitoriasAndPartidas() throws Exception {
    
        Integer id = 26;
        Integer qtdeVitorias = 19;
        Integer qtdePartidas = 19;
        mockMvc.perform(put(this.urlBase+"/"+id.toString()+"?qdteVitorias="+qtdeVitorias.toString()+"&qdtePartidas="+qtdePartidas.toString()))
        .andExpect(status().isCreated());
    }
}
